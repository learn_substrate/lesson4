// practive 1
// use std::io;
// fn main() {
//     let mut input: Vec<String>;
//     loop {
//         let mut input_text = String::new();
//         println!("Type instruction in the format Add <name> to <department>:");
//         io::stdin()
//             .read_line(&mut input_text)
//             .expect("failed to read from stdin");
//         let trimmed_text: String = input_text.trim().to_string();

//         input = trimmed_text.split(" ").map(|x| x.to_string()).collect();
//         println!("trimmed_text {:?} ", input);
//         if input[0] == "Add" && input[2] == "to" {
//             break;
//         } else {
//             println!("Invalid format.");
//         }
//     }
//     println!("{:?}", input);
// }

//practive 2
// trait AppendBar {
//     fn append_bar(self) -> Self;
// }

// impl AppendBar for String {
//     //Add your code here
//     fn append_bar(self) -> Self {
//         self + "Bar"
//     }
// }

// fn main() {
//     let s = String::from("Foo");
//     let s = s.append_bar();
//     println!("s: {}", s);
// }

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn is_foo_bar() {
//         assert_eq!(String::from("Foo").append_bar(), String::from("FooBar"));
//     }

//     #[test]
//     fn is_bar_bar() {
//         assert_eq!(
//             String::from("").append_bar().append_bar(),
//             String::from("BarBar")
//         );
//     }
// }

//practive 3
// trait AppendBar {
//     fn append_bar(self) -> Self;
// }
// //TODO: Add your code here
// impl AppendBar for Vec<String> {
//     fn append_bar(self) -> Self {
//         let mut _vecBar = self;
//         _vecBar.push("Bar".to_string());
//         _vecBar
//     }
// }

// fn main() {}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn is_vec_pop_eq_bar() {
//         let mut foo = vec![String::from("Foo")].append_bar();
//         assert_eq!(foo.pop().unwrap(), String::from("Bar"));
//         assert_eq!(foo.pop().unwrap(), String::from("Foo"));
//     }
// }

// ==== Exercise 1: trait ====

use std::iter::Iterator;
#[derive(Debug)]
struct Fibonacci {
    a: u32,
    b: u32,
}

impl Iterator for Fibonacci {
    type Item = u32;
    fn next(&mut self) -> Option<u32> {
        let new_next = self.b + self.a;

        self.b = self.a;
        self.a = new_next;

        Some(new_next)
    }
}
fn fibonacci_numbers() -> Fibonacci {
    Fibonacci { a: 1, b: 0 }
}

fn main() {
    for i in fibonacci_numbers().take(13) {
        println!("😀 🦀 {}", i);
    }
}

// ==== Exercise 2: Lifetime ====

// use std::fmt;
// struct StrDisplayable<'a>(Vec<&'a str>);

// impl fmt::Display for StrDisplayable<'static> {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         for v in &self.0 {
//             write!(f, "\n{}", v);
//         }
//         Ok(())
//     }
// }

// fn main() {
//     let vec: Vec<&str> = vec!["a", "bc", "def"];
//     let vec_Foo = StrDisplayable(vec);
//     println!("{}", vec_Foo);
// }
